(defun my-but-last (xs)
  (if (null (cddr xs))
      xs
      (my-but-last (cdr xs))))
