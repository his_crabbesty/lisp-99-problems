(defun my-list-length (xs)
  (if (null xs)
      0
      (1+ (my-list-length (cdr xs)))))
