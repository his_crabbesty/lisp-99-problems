(defun my-reverse (xs)
  (let ((ys nil))
    (dolist (x xs) (push x ys))
    ys))
