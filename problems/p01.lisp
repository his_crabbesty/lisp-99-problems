(defun my-last (xs)
  (if (null (cdr xs)) 
      xs
      (my-last (cdr xs))))
