(defun compress (xs)
   (destructuring-bind (&optional (a nil a-supplied-p) (b nil b-supplied-p) &rest rest) xs
     (declare (ignore rest))
     (cond ((not a-supplied-p) nil)
	   ((not b-supplied-p) (cons a nil))
	   ((equal a b) (compress (cdr xs)))
	   (t (cons a (compress (cdr xs)))))))
