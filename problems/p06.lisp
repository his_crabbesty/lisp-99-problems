(defun palindrome-p (xs)
  (equal xs (reverse xs)))
